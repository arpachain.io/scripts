#!/bin/bash
echo '' >> ~/.bashrc
echo 'export GOROOT=/usr/local/go' >> ~/.bashrc
echo 'export GOPATH=~/go' >> ~/.bashrc
echo 'export SOLCPATH=/snap/bin' >> ~/.bashrc
echo 'export PATH=$PATH:$GOPATH/bin:$GOROOT/bin:~/protoc-3.6.1/bin:$SOLCPATH' >> ~/.bashrc
export GOPATH=~/go
export GOROOT=/usr/local/go
export PATH=$PATH:$GOPATH/bin:$GOROOT/bin:~/protoc-3.6.1/bin:$SOLCPATH
echo '# this goes at the end of your $HOME/.bashrc file' >> ~/.bashrc
echo 'export mylocal="$HOME/local"' >> ~/.bashrc
echo '# export OpenSSL paths' >> ~/.bashrc
echo 'export PATH="${mylocal}/openssl/bin/:${PATH}"' >> ~/.bashrc
echo 'export C_INCLUDE_PATH="${mylocal}/openssl/include/:${C_INCLUDE_PATH}"' >> ~/.bashrc
echo 'export CPLUS_INCLUDE_PATH="${mylocal}/openssl/include/:${CPLUS_INCLUDE_PATH}"' >> ~/.bashrc
echo 'export LIBRARY_PATH="${mylocal}/openssl/lib/:${LIBRARY_PATH}"' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH="${mylocal}/openssl/lib/:${LD_LIBRARY_PATH}"' >> ~/.bashrc
echo '# export MPIR paths' >> ~/.bashrc
echo 'export PATH="${mylocal}/mpir/bin/:${PATH}"' >> ~/.bashrc
echo 'export C_INCLUDE_PATH="${mylocal}/mpir/include/:${C_INCLUDE_PATH}"' >> ~/.bashrc
echo 'export CPLUS_INCLUDE_PATH="${mylocal}/mpir/include/:${CPLUS_INCLUDE_PATH}"' >> ~/.bashrc
echo 'export LIBRARY_PATH="${mylocal}/mpir/lib/:${LIBRARY_PATH}"' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH="${mylocal}/mpir/lib/:${LD_LIBRARY_PATH}"' >> ~/.bashrc
echo '' >> ~/.bashrc

#!/bin/bash
# use proxy for ssh connect to git
# sudo apt-get install netcat-openbsd
# add folder .ssh
# create file config in .ssh
# copy content to config file
# Host github.com
#   HostName github.com
#   User git
#   ProxyCommand nc -v -x 192.168.0.164:1080 %h %p
#
# run this script after `env_prepare.sh`, and in a new terminal
set -ex
# install required tools, gcc-7, and g++-7
sudo apt-get update \
&& sudo apt-get --yes install apt-utils \
&& sudo apt-get --yes install unzip \
&& sudo apt-get --yes install wget \
&& sudo apt-get --yes install curl \
&& sudo apt-get --yes install bzip2 \
&& sudo apt-get --yes install git \
&& sudo apt-get --yes install bash-completion \
&& sudo apt-get --yes install clang-format \
&& sudo apt-get --yes install build-essential autoconf libtool pkg-config \
&& sudo apt-get --yes install software-properties-common \
&& sudo add-apt-repository 'deb http://deb.debian.org/debian buster main' \
&& sudo apt-get update \
&& sudo apt-get --yes install gcc-7 \
&& sudo apt-get --yes install g++-7 \
&& sudo rm /usr/bin/cpp /usr/bin/gcc /usr/bin/g++ \
&& sudo ln -s /usr/bin/cpp-7 /usr/bin/cpp \
&& sudo ln -s /usr/bin/gcc-7 /usr/bin/gcc \
&& sudo ln -s /usr/bin/g++-7 /usr/bin/g++ \
&& gcc -v
# install go, go tools, dep, and protoc-gen-go plugin
sudo apt-get update \
&& mkdir -p /usr/local/ \
&& cd \
&& wget https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz \
&& sudo tar -C /usr/local -xzf go1.11.2.linux-amd64.tar.gz \
&& go version \
&& go env \
&& go tool \
&& go get -u -v google.golang.org/grpc \
&& cd \
&& wget https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip \
&& unzip protoc-3.6.1-linux-x86_64.zip -d protoc-3.6.1 \
&& protoc --version \
&& go get -u -v github.com/golang/protobuf/protoc-gen-go \
&& go get -u -v github.com/golang/dep/cmd/dep \
&& dep version
# install mockgen
sudo apt-get update \
&& go get -u -v github.com/golang/mock/gomock \
&& cd $GOPATH/src/github.com/golang/mock/mockgen \
&& go build \
&& mv mockgen $GOPATH/bin
# install rs/xid
go get -u -v github.com/rs/xid
# install grpcpool
go get -u -v github.com/processout/grpc-go-pool
# set a folder with name local
cd \
&& mylocal="$HOME/local" \
&& mkdir -p ${mylocal} \
&& cd ${mylocal}
# install openssl 1.1.0j
cd \
&& sudo apt-get update \
&& wget https://www.openssl.org/source/openssl-1.1.0j.tar.gz \
&& tar -xvf openssl-1.1.0j.tar.gz \
&& cd openssl-1.1.0j \
&& ./config --prefix="${mylocal}/openssl" \
&& make \
&& sudo make install \
&& sudo ldconfig
# install gRPC and protobuf, prerequest openssl
cd \
&& sudo apt-get update \
&& sudo apt-get --yes install libgflags-dev libgtest-dev \
&& sudo apt-get --yes install clang libc++-dev \
&& cd \
&& git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc \
&& cd grpc \
&& git submodule update --init \
&& sudo make \
&& sudo make install \
&& sudo make grpc_cli \
&& sudo ln -s /$USER/grpc/bins/opt/grpc_cli /usr/bin/grpc_cli \
&& cd third_party/protobuf \
&& sudo make install
# install utils including glog and gflag
sudo apt-get update \
&& sudo apt-get --yes install cmake \
&& cd \
&& git clone https://github.com/gflags/gflags \
&& cd gflags \
&& mkdir build \
&& cd build \
&& cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON -G"Unix Makefiles" .. \
&& make \
&& sudo make install \
&& sudo ldconfig \
&& cd \
&& git clone https://github.com/google/glog \
&& cd glog \
&& mkdir build \
&& cd build \
&& cmake -D BUILD_gflags_LIBS=ON -D BUILD_SHARED_LIBS=ON -D BUILD_gflags_nothreads_LIBS=ON -D GFLAGS_NAMESPACE=ON .. \
&& make \
&& sudo make install \
&& sudo ldconfig
# install eth nodejs solc
# REMEMBER TO ADD SOLCPATH=/snap/bin
sudo apt-get update \
&& sudo apt-get --yes install snapd \
&& sudo snap install solc \
&& sudo apt-get install curl software-properties-common -y \
&& curl -sL https://deb.nodesource.com/setup_10.x | sudo bash - \
&& sudo apt-get install -y nodejs

# install abigen from go-ethereum repo
sudo apt-get update \
&& go get -u -v github.com/ethereum/go-ethereum \
&& cd $GOPATH/src/github.com/ethereum/go-ethereum/ \
&& make \
&& make devtools
# install prerequisite for SCALE-MAMBA (yasm, mpir, gmpy2, openssl)
# build and test-run SCALE-MAMBA
sudo apt-get update \
&& sudo apt-get install libboost-filesystem-dev -y \
&& sudo apt-get --yes install yasm
# install MPIR
cd ${mylocal} \
&& sudo apt-get update \
&& wget http://mpir.org/mpir-3.0.0.tar.bz2 \
&& tar -xvf mpir-3.0.0.tar.bz2 \
&& cd mpir-3.0.0 \
&& ./configure --enable-cxx --prefix="${mylocal}/mpir" \
&& make \
&& make check \
&& sudo make install
#Install GMP
cd ~ \
&& wget https://gmplib.org/download/gmp/gmp-6.1.2.tar.bz2 \
&& tar xvjf gmp-6.1.2.tar.bz2 \
&& cd gmp-6.1.2 \
&& ./configure && make && make check && sudo make install
#Install MPFR
cd ~ \
&& wget https://www.mpfr.org/mpfr-3.1.2/mpfr-3.1.2.tar.bz2 \
&& tar xvjf mpfr-3.1.2.tar.bz2 \
&& cd mpfr-3.1.2 \
&& ./configure --with-gmp-include=/usr/local/include --with-gmp-lib=/usr/local/lib && make && make check && sudo make install
#Install MPC
cd ~ \
&& wget https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz \
&& tar zxvf mpc-1.1.0.tar.gz \
&& cd mpc-1.1.0 \
&& ./configure && make && make check && sudo make install
sudo apt install python-pip -y
pip install gmpy2
# install crypto++ for v1.3
sudo apt-get install libcrypto++-dev libcrypto++-doc libcrypto++-utils -y
# use scale mamba to test
cd \
&& git clone https://github.com/KULeuven-COSIC/SCALE-MAMBA.git scale \
&& cd scale \
# Force to checkout scale version 1.3
&& git checkout 862ecf547a01883cfbaf81a07c444c0c7cb53010 \
&& cp CONFIG CONFIG.mine \
&& echo 'ROOT = /$HOME/scale' >> CONFIG.mine \
&& echo 'OSSL = /${mylocal}/openssl' >> CONFIG.mine \
&& make progs
# test installation
cd ~ \
&& cd scale \
&& cp Auto-Test-Data/Cert-Store/* Cert-Store/ \
&& cp Auto-Test-Data/1/* Data/ \
&& Scripts/test.sh test_sqrt

# ARPA MPC gcloud server installation
## How to use ?

 - start gcloud instance with `Debian GNU/Linux 9 (stretch)`
 - open terminal upload file `env_prepare.sh` and `auto_install.sh`
 - run `env_prepare.sh` by commande
    + `bash env_prepare.sh`
 - close terminal, let `.bashrc` reload
 - open new termianl, run commande
    + `bash auto_install.sh`

YOU NEED TO MANNUAL DO ONE THIND:  
When terminal show gcc compiler replace, use `Tab` to choose **Yes**, type `Enter`  

Voila! Take a coffee, it will cost about 30 mins with micro server.  

At the end it will automatic run `Scripts/test.sh test_sqrt`, if all things good, result like below:
```bash
...
test_sqrt-0 needs more integer memory, resizing to 8192
test_sqrt-0 needs more gfp memory, resizing to 20000
test_sqrt-0 needs more Share memory, resizing to 8192
test_sqrt-0 needs more integer memory, resizing to 8192
All connections now done
Setting up threads
All connections now done
Setting up threads
Waiting for thread 0 to be ready
Waiting for thread 0 to be ready
I am player 0 in thread 4
I am player 0 in thread 2
I am player 0 in thread 3
I am player 1 in thread 3
I am player 1 in thread 1
I am player 0 in thread 0
I am player 1 in thread 0
I am player 0 in thread 1
I am player 1 in thread 2
I am player 1 in thread 4
Set up player 0 in thread 1 
Set up player 0 in thread 4 
Doing online for player 0 in online thread 0
Set up player 0 in thread 2 
Set up player 0 in thread 3 
Set up player 0 in thread 0 
Set up player 1 in thread 4 
Doing online for player 1 in online thread 0
Set up player 1 in thread 2 
Set up player 1 in thread 3 
Set up player 1 in thread 1 
Set up player 1 in thread 0 
Signal online thread ready 0
Signal online thread ready 0
Starting online phase
test sqrt:
Compiler: /home/shaolong8633/scale/Scripts/../compile.py --dead-code-elimination Programs/test_sqrt
Waiting for all clients to finish
Starting online phase
Compiler: /home/shaolong8633/scale/Scripts/../compile.py --dead-code-elimination Programs/test_sqrt
Waiting for all clients to finish
        Thread 0 terminating
Waiting for all clients to finish
        Thread 0 terminating
Waiting for all clients to finish
Sent 3222 elements in 79 rounds
Sent 3222 elements in 79 rounds
Exiting online phase : 0
Exiting square phase: thread = 0
Exiting bit phase: thread = 0
Exiting sacrifice phase : thread = 0
Exiting mult phase : thread = 0
End of prog
Exiting online phase : 0
Exiting square phase: thread = 0
Exiting bit phase: thread = 0
Exiting sacrifice phase : thread = 0
Exiting mult phase : thread = 0
End of prog
p = 340282366920938463463374607431768211507
param = 136
byte_length = 24
9 1.414215087890625
10 5.999908447265625
11 5.0002098083496094
12 7.0002536773681641
...
```
